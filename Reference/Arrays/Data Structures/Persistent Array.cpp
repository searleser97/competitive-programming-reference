// 3
struct PersistentArray {
  int version;
  vector<map<int, int>> arr;
  // 4
  PersistentArray(int n) {
    version = 0;
    arr.assign(n, map<int, int>({{0, 0}}));
  }
  // O(lg(N))
  void set(int i, int val) { arr[i][version] = val; }

  // O(1)
  int save() { return version++; }
  // 5
  // O(lg(N))
  int get(int i, int versionId) {
    auto it = arr[i].upper_bound(versionId);
    return prev(it)->second;
  }
  // 5
  // O(lg(N))
  void update(int i, int versionId, int val) {
    arr[i][versionId] = val;
  }
};