// 12
template<class T>
void permutationsDFS(vector<T>& v, int L, vector<vector<T>>& ans) {
  if (L == v.size() - 1) {
    ans.push_back(v);
    return;
  }
  for (int i = L; i < v.size(); i++) {
    swap(v[L], v[i]);
    permutationsDFS(v, L + 1, ans);
    swap(v[L], v[i]);
  }
}
// 7
// O(N!)
template<class T>
vector<vector<T>> permutations(vector<T>& v) {
  vector<T> ans;
  permutationsDFS(v, 0, ans);
  return ans;
}
