// 8
template <class T>
T bitsToInt(string bits) {
  assert(bits.size() <= sizeof(T) * 8);
  T ans = 0;
  for (int i = 0, j = bits.size() - 1; ~j; i++, j--)
    ans |= (T)(bits[i] - '0') << j;
  return ans;
}