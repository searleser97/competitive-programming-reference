// 7
template<class T>
string toBitsString(T n) {
  string ans = "";
  for (int i = sizeof(n) * 8 - 1; ~i; i--)
    ans += ((n >> i) & 1) + '0';
  return ans;
}