# 12
from random import randint

n = randint(2, 10)

cout = f"{n}\n"

for i in range(2, n + 1):
    u = i
    v = randint(1, i - 1)
    weight = randint(1, 30)
    cout += f"{u} {v} {weight}\n"
print(cout)