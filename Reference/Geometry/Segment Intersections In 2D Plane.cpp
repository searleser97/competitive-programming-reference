#include <bits/stdc++.h>
using namespace std;
// 6
#include "Data Structures/Segment.cpp"
#include "Segment-Segment Intersection.cpp"

struct Event {
  Point p;
  vector<Segment> segs;
  int type;

  Event(Point &p, vector<Segment> &segs, int type)
      : p(p), type(type), segs(segs) {}

  bool operator<(const Event &e) {
    return this->p.x == e.p.x ? this->p.y > e.p.y
                              : this->p.x < e.p.x;
  }
};

ld getY(Point &a, Point &b, ld x) {
  if (eq(a.x, b.x)) return a.y;
  return a.y + (b.y - a.y) * (x - a.x) / (b.x - a.x);
}

vector<Point> allSegIntersections2D(
    vector<Segment> &segs) {}