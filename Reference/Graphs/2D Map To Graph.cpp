/*
Example: https://gitlab.com/searleser97/algorithms/-/
blob/master/Contests/PracticaRepechaje2021/
UCFLOCALS2017/d.cpp
*/
// 4
vector<vector<int>> adj,
    movs = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
vector<vector<char>> mat;
vector<int> acum;
// 7
int isValid(pair<int, int> u) {
  if (u.first < 0 or u.first >= (int)mat.size() or
      u.second < 0 or
      u.second >= (int)mat[u.first].size())
    return -1;  // invalid position
  return 1;  // completely valid position
}
// 6
int getId(pair<int, int> u, int dir = -1) {
  switch (isValid(u)) {
    case 1: return acum[u.first] + u.second;
    default: return -1;
  }
}
// 7
void addEdge(int u, int v) { adj[u].push_back(v); }

void addEdge(pair<int, int> u, pair<int, int> v,
             int dir) {
  int uId = getId(u), vId = getId(v, dir);
  if (uId != -1 && vId != -1) addEdge(uId, vId);
}
// 11
void toGraph() {
  acum = vector<int>(mat.size() + 1);
  for (int i = 1; i <= mat.size(); i++)
    acum[i] = mat[i - 1].size() + acum[i - 1];
  adj = vector<vector<int>>(acum.back());
  for (int i = 0; i < mat.size(); i++)
    for (int j = 0; j < mat[i].size(); j++)
      for (int k = 0; k < movs.size(); k++)
        addEdge({i, j},
                {i + movs[k][0], j + movs[k][1]}, k);
}