// 6
// APB = articulation points and bridges
// Ap = Articulation Point
// br = bridges, p = parent
// disc = discovery time
// low = lowTime, ch = children
// nup = number of edges from u to p
// 4
int Time;
vector<vector<int>> adj;
vector<int> disc, low, isAp;
vector<pair<int, int>> br;

void init(int N) { adj = vector<vector<int>>(N); }
// 3
void addEdge(int u, int v) {
  adj[u].push_back(v) adj[v].push_back(u);
}
// 15
int dfsAPB(int u, int p) {
  int ch = 0, nup = 0;
  low[u] = disc[u] = ++Time;
  for (int &v : adj[u]) {
    if (v == p && !nup++) continue;
    if (!disc[v]) {
      ch++, dfsAPB(v, u);
      if (disc[u] <= low[v]) isAp[u]++;
      if (disc[u] < low[v]) br.push_back({u, v});
      low[u] = min(low[u], low[v]);
    } else
      low[u] = min(low[u], disc[v]);
  }
  return ch;
}
// 7
// O(V + E)
void APB() {
  br.clear(), Time = 0;
  isAp = low = disc = vector<int>(adj.size());
  for (int u = 0; u < adj.size(); u++)
    if (!disc[u]) isAp[u] = dfsAPB(u, u) > 1;
}