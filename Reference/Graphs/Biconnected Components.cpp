// 4
// bi = biconnected components, p = parent
// disc = discovery time, st = stack
// low = lowTime, ch = children
// nup = number of edges from u to p
// 5
int Time;
vector<vector<int>> adj, bicomps;
vector<int> disc, low, st;
vector<pair<int, int>> stEdges;
vector<vector<pair<int, int>>> bicompsEdges;

void init(int N) { adj = vector<vector<int>>(N); }

// 3
void addEdge(int u, int v) {
  adj[u].push_back(v), adj[v].push_back(u);
}
// 32
int biDFS(int u, int p) {
  int ch = 0, nup = 0;
  low[u] = disc[u] = ++Time, st.push_back(u);
  for (int &v : adj[u]) {
    if (v == p && !nup++) continue;
    if (!disc[v] or disc[u] > disc[v])
      stEdges.push_back({u, v});
    if (!disc[v]) {
      int stlen = st.size();
      ++ch, biDFS(v, u);
      if (disc[u] <= low[v]) {
        vector<int> bcomp = {u};
        while (stlen < st.size()) {
          bcomp.push_back(st.back());
          st.pop_back();
        }
        bicomps.push_back(bcomp);
        vector<pair<int, int>> edges;
        pair<int, int> thisEdge = {u, v};
        while (edges.empty() or
               edges.back() != thisEdge) {
          edges.push_back(stEdges.back());
          stEdges.pop_back();
        }
        bicompsEdges.push_back(edges);
      }
      low[u] = min(low[u], low[v]);
    } else
      low[u] = min(low[u], disc[v]);
  }
  return ch;
}
// 15
// O(V + E)
void biComponents() {
  low = disc = vector<int>(adj.size());
  bicomps.clear(), bicompsEdges.clear();
  Time = 0;
  for (int u = 0; u < adj.size(); u++) {
    if (!disc[u]) {
      st.clear(), stEdges.clear();
      if (biDFS(u, u) == 0) {
        bicomps.push_back({u});
        bicompsEdges.push_back({});
      }
    }
  }
}