// 5
#include "Strongly Connected Components.cpp"

void addEdge(int u, int v) {
  adj[u].push_back(v), adj[v].push_back(u);
}
// 5
// O(V + E)
vector<vector<int>> bridgeComps() {
  SCCS(0);
  return sccs;
}