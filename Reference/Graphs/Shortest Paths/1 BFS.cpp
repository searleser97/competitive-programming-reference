// 7
// s = source
typedef int T;  // sum of costs might overflow
vector<vector<int>> adj;

void init(int N) { adj = vector<vector<int>>(N); }
// Assumes Directed Graph
void addEdge(int u, int v) { adj[u].push_back(v); }
// 14
// O(E)
vector<T> sssp1(int s) {
  vector<T> dist(adj.size(), -1);
  deque<int> q;
  dist[s] = 0, q.push_front(s);
  while (q.size()) {
    int u = q.front();
    q.pop_front();
    for (auto& v : adj[u])
      if (dist[v] == -1)
        dist[v] = dist[u] + 1, q.push_back(v);
  }
  return dist;
}