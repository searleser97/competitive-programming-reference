// 5
// tv = top value from stack, Dir = is directed graph
// sccs = strongly connected components
// scc = strongly connected component
// disc = discovery time, low = low time
// s = stack, top = top index of the stack
// if isDir = false we get bridge connected components
// 5
int Time, top;
vector<vector<int>> adj, sccs;
vector<int> disc, low, s;

void init(int N) { adj.assign(N, vector<int>()); }

void addEdge(int u, int v) { adj[u].push_back(v); }
// 18
void dfsSCCS(int u, int p, bool isDir = 1) {
  if (disc[u]) return;
  low[u] = disc[u] = ++Time;
  s[++top] = u;
  for (int &v : adj[u])
    if (isDir || (!isDir && v != p))
      dfsSCCS(v), low[u] = min(low[u], low[v]);
  if (disc[u] == low[u]) {
    vector<int> scc;
    while (true) {
      int tv = s[top--];
      scc.push_back(tv);
      low[tv] = adj.size();
      if (tv == u) break;
    }
    sccs.push_back(scc);
  }
}
// 7
// O(V + E)
void SCCS(bool isDir = 1) {
  s = low = disc = vector<int>(adj.size());
  Time = 0, top = -1, sccs.clear();
  for (int u = 0; u < adj.size(); u++)
    dfsSCCS(u, u, isDir);
}