// 11
#include "../Number Theory/Modular Multiplicative Inverse.cpp"
#include "Factorials Modulo M.cpp"

// O(lg(p))
li nCk(int n, int k, li p) {
  if (fact.size() <= n) computeFact(n, p);
  if (n < k) return 0;
  return fact[n] * modInv(fact[k], p) % p *
         modInv(fact[n - k], p) % p;
}
