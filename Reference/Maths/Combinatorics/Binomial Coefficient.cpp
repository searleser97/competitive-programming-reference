// 8
// O(k)
li nCk(li n, int k) {
  if (n < k) return 0;
  double ans = 1;
  for (int i = 1; i <= k; i++)
    ans = ans * (n - k + i) / i;
  return (li)(ans + 0.01);
}