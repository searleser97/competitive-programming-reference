// 7
vector<li> fact;

void computeFact(int n, li m) {
  int i = max(1, int(fact.size()));
  fact.resize(++n), fact[0] = 1;
  for (; i < n; i++) fact[i] = fact[i - 1] * i % m;
}