// 7
#include "Extended Euclidean Algorithm.cpp"
// ax + by = c = gcd(a, b)
pair<li, li> diophantineBase(li a, li b, li c) {
  auto [d, x, y] = extendedGCD(a, b);
  if (c % d) return {0, 0};
  return {c / d * x, c / d * y};
}