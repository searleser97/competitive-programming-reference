// 10
#include "Extended Euclidean Algorithm.cpp"
// ax + by = c
// returns the i-th solution or {0, 0} if no solutions
pair<li, li> diophantineSol(li a, li b, li c, li i) {
  auto [d, x, y] = extendedGCD(a, b);
  if (c % d) return {0, 0};
  return {c / d * x + b / d * i,
          c / d * y - a / d * i};
}