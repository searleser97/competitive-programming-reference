// 12
// divs = divisors

// O(sqrt(N))
vector<li> getDivisors(li n) {
  vector<li> divs;
  for (li i = 1; i * i <= n; i++)
    if (!(n % i)) {
      divs.push_back(i);
      if (i * i != n) divs.push_back(n / i);
    }
  return divs;
}