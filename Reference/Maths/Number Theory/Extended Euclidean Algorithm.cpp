// 9
#include "../../Util/Floor Of Division Between Integers.cpp"
#include "../../Util/Modulo with negative numbers.cpp"

// gcd(a, b) = ax + by
array<li, 3> extendedGCD(li a, li b) {
  if (!a) return {b, 0, 1};
  auto [d, x, y] = extendedGCD(mod(b, a), a);
  return {d, y - floor(b, a) * x, x};
}  // {gcd(a, b), x, y}