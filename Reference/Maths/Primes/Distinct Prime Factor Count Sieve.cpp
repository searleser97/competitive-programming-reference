// 12
#include "Primes Sieve.cpp"

vector<int> distinctPFCount;
 
// O(N)
void distinctPrimeFactorCountSieve(int n) {
  if (sieve.size() < n + 1) throw "sieve not big enough";
  distinctPFCount.assign(n + 1, 0);
  for (int i = 2; i <= n; i++) {
    int j = i / minPrimeDiv(i);
    distinctPFCount[i] = distinctPFCount[j] + (minPrimeDiv(i) != minPrimeDiv(j));
  }
}
