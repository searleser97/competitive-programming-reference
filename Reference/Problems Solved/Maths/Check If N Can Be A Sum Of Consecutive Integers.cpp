// 7
#include "../../BITs Manipulation/Is Power Of 2.cpp"

// Checks if a number can be expressed as a sum of 2
// or more consecutive integers
bool canBeConsecutiveSum(int n) {
  return !isPowerOf2(n);
}