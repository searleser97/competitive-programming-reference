// 6
// example:
// sequence = 12345678910111213141516...
// digit index = 19
// answer = 4 -> corresponding to the second digit of
// number "14"
#define li long long int
// 11
li getIndex(li n) {
  int exp = 1;
  li tens = 1, ncpy = n / 10, idx = 0;
  while (ncpy) {
    ncpy /= 10;
    idx += 9 * tens * exp;
    tens *= 10;
    exp++;
  }
  return idx + exp * (n - tens) + 1;
}
// 10
char getDigit(li idx) {
  li l = 1, r = 1e12;
  while (l <= r) {
    li mid = l + (r - l) / 2;
    if (getIndex(mid) <= idx) l = mid + 1;
    else
      r = mid - 1;
  }
  li localIdx = idx - getIndex(l - 1);
  return to_string(l - 1)[localIdx];
}