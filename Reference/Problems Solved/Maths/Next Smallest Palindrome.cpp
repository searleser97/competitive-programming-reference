// 5
bool is9(string &str, int pos) {
  if (pos < 0 or pos >= str.size() or str[pos] != '9')
    return false;
  return true;
}
// 15
// O(N)
string nextSmallestPalindrome(string A) {
  string bkp = A;
  int l = 0, r = A.size() - 1;
  while (l <= r) A[r] = A[l], l++, r--;
  if (A > bkp) return A;
  l--, r++;
  while (is9(A, l)) A[l] = A[r] = '0', l--, r++;

  if (l >= 0) A[r] = ++A[l];
  else
    A[0] = '1', A += '1';

  return A;
}