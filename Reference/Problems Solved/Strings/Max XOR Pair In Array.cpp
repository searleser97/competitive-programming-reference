// 22
#include "../../BITs Manipulation/Bits To Int.cpp"
#include "../../BITs Manipulation/Int To Bits String.cpp"

struct Trie {
  struct Node {
    Node *ch[2];
  };

  Node *root;

  Trie() { root = new Node(); }

  void insert(string str) {
    Node *curr = root;
    for (auto &c : str) {
      if (curr->ch[c - '0'] == nullptr)
        curr->ch[c - '0'] = new Node();
      curr = curr->ch[c - '0'];
    }
  }
};
// 15
// O(s.size())
string queryMaxXOR(string s, Trie &t) {
  auto *curr = t.root;
  string ans;
  for (auto &c : s) {
    if (curr->ch['1' - c] != nullptr) {
      ans += '1';
      curr = curr->ch['1' - c];
    } else {
      ans += '0';
      curr = curr->ch[c - '0'];
    }
  }
  return ans;
}
// 12
// O(N * sizeof(v[i]))
template <class T>
T maxXORPair(vector<T> &v) {
  Trie t;
  for (auto &e : v) { t.insert(toBitsString<T>(e)); }
  T ans = 0;
  for (auto &e : v) {
    auto q = queryMaxXOR(toBitsString<T>(e), t);
    ans = max(ans, bitsToInt<T>(q));
  }
  return ans;
}