// 8
// As an example of how to use Mo's this code counts
// the number of distinct elements in range [L, R]

template <class T>
struct Query {
  int l, r, idx;
  T result;
};
// 37
// O((Q + N) * sqrt(N))
template <class T>
void calcQueries(vector<int> &v,
                 vector<Query<T>> &qs) {
  for (int i = 0; i < qs.size(); i++) qs[i].idx = i;
  int blk = (int)sqrt(v.size());
  sort(qs.begin(), qs.end(),
      [&](Query<T> a, Query<T> b) {
        int blka = a.l / blk, blkb = b.l / blk;
        return blka == blkb ? a.r < b.r : blka < blkb;
      });
  int currL = 0, currR = 0, distinct_elements = 0;
  vector<int> freq(1e5 + 13);
  for (auto &q : qs) {
    while (currL < q.l) {
      if (!--freq[v[currL]]) distinct_elements--;
      currL++;
    }
    while (currL > q.l) {
      currL--;
      if (++freq[v[currL]] == 1) distinct_elements++;
    }
    while (currR <= q.r) {
      if (++freq[v[currR]] == 1) distinct_elements++;
      currR++;
    }
    while (currR > q.r + 1) {
      currR--;
      if (!--freq[v[currR]]) distinct_elements--;
    }
    q.result = distinct_elements;
  }
  sort(qs.begin(), qs.end(),
      [&](Query<T> a, Query<T> b) {
        return a.idx < b.idx;
      });
}