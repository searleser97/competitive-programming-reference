// 3
template <class T>
struct PrefixArray {
  vector<T> prefix;
  // 5
  PrefixArray(vector<T> v) {
    prefix = v;
    for (int i = 0; i < v.size(); i++)
      prefix[i] += query(i - 1);
  }
  // 3
  T query(int l, int r) {
    return query(r) - query(l - 1);
  }
  // 2
  T query(int i) { return i < 0 ? 0 : prefix[i]; }
};