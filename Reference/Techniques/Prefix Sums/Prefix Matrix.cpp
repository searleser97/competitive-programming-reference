// 3
template <class T>
struct PrefixMatrix {
  vector<vector<T>> prefix;
  // 7
  PrefixMatrix(vector<vector<T>> matrix) {
    prefix = matrix;
    for (int i = 0; i < matrix.size(); i++)
      for (int j = 0; j < matrix[i].size(); j++)
        prefix += query(i, j - 1) + query(i - 1, j) -
                  query(i - 1, j - 1);
  }
  // 4
  T query(int r1, int c1, int r2, int c2) {
    return query(r2, c2) - query(r2, c1 - 1) -
           query(r1 - 1, c2) + query(r1 - 1, c1 - 1);
  }
  // 4
  T query(int r, int c) {
    return (r < 0 or c < 0) ? 0 : prefix[r][c];
  }
};