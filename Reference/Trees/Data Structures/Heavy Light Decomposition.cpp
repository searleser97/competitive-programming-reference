// 8
#include "../Ranges/Data Structures/Segment Tree.cpp"
template <class T>
struct HLD {
  SegmentTree<T> st;
  function<T(T, T)> F;
  vector<int> dad, heavy, depth, root, stPos, iStPos;
  bool inEdges; // false -> for values in nodes
  T neutro;
  // 24
  // O(N)
  HLD(vector<vector<int>>& adj, T neut, bool edges,
      T f(T, T) = [](T a, T b) { return a + b; })
      : F(f), heavy(adj.size(), -1), inEdges(edges) {
    dad = root = stPos = iStPos = depth = heavy;
    st = SegmentTree<T>(adj.size(), neut, f);
    function<int(int)> dfs = [&](int u) {
      int size = 1, maxSubtree = 0;
      for (int& v : adj[u]) {
        if (v == dad[u]) continue;
        dad[v] = u, depth[v] = depth[u] + 1;
        int subtree = dfs(v);
        if (subtree > maxSubtree)
          heavy[u] = v, maxSubtree = subtree;
        size += subtree;
      }
      return size;
    };
    dad[0] = -1, depth[0] = 0, dfs(0);
    for (int i = 0, pos = 0; i < adj.size(); i++)
      if (dad[i] < 0 || heavy[dad[i]] != i)
        for (int j = i; ~j; j = heavy[j])
          iStPos[stPos[j] = pos++] = j, root[j] = i;
  }
  // 11
  // O(lg(N) * O(op))
  int lca(int u, int v,
      function<void(T, T)> op = [](T, T) {}) {
    for (; root[u] != root[v]; v = dad[root[v]]) {
      if (depth[root[u]] > depth[root[v]]) swap(u, v);
      op(stPos[root[v]], stPos[v]);
    }
    if (depth[u] > depth[v]) swap(u, v);
    op(stPos[u] + inEdges, stPos[v]);
    return u;
  }
  // 10
  // O(lg^2 (N))
  void update(int u, int v, int val) {
    /* for single node or edge update */
    if (depth[u] > depth[v]) swap(u, v);
    st.update(stPos[v], val);
    /* for update in a range of edges or nodes 
    lca(u, v, [&](int l, int r) {
      st.update(l, r, val);
    }); */ // for this case use segment tree lazy
  }
  // 8
  // O(lg^2 (N))
  T query(int u, int v) {
    T ans = neutro;
    lca(u, v, [&](int l, int r) {
      ans = F(ans, st.query(l, r));
    });
    return ans;
  }
  // 9
  // O(lg(N))
  // kth node in the path from u to root
  int kth(int u, int k) {
    for (; stPos[u] - stPos[root[u]] < k;
         u = dad[root[u]])
      k -= stPos[u] - stPos[root[u]] + 1;
    return iStPos[stPos[u] - k];
  }
};