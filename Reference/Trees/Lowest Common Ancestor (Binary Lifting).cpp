// 6
// tin = time in, tout = time out, p = parent
int Time, logn;
vector<vector<int>> adj, up;
vector<int> tin, tout;

void init(int N) { adj.assign(N, vector<int>()); }
// 3
void addEdge(int u, int v) {
  adj[u].push_back(v), adj[v].push_back(u);
}
// 8
void dfs(int u, int p) {
  tin[u] = ++Time, up[u][0] = p;
  for (int i = 1; i <= logn; ++i)
    up[u][i] = up[up[u][i - 1]][i - 1];
  for (int v : adj[u])
    if (v != p) dfs(v, u);
  tout[u] = ++Time;
}
// 7
// O(lg(N))
void preprocess(const vector<int> &roots) {
  Time = 0, logn = ceil(log2(adj.size()));
  tin = tout = vector<int>(adj.size());
  up.assign(adj.size(), vector<int>(logn + 1));
  for (auto &root : roots) dfs(root, root);
}
// 3
bool isAncestor(int u, int v) {  // is u ancestor of v
  return tin[u] <= tin[v] && tout[u] >= tout[v];
}
// 8
// O(lg(N))
int lca(int u, int v) {
  if (isAncestor(u, v)) return u;
  if (isAncestor(v, u)) return v;
  for (int i = logn; ~i; --i)
    if (!isAncestor(up[u][i], v)) u = up[u][i];
  return up[u][0];
}
