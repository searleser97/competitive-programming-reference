// 7
// p = parent, tin = time in
#include "../Ranges/Data Structures/Sparse Table.cpp"
typedef pair<int, int> pairii;
vector<int> tin;
vector<pair<int, int>> tour;
vector<vector<int>> adj;
SparseTable<pairii> st;

void init(int N) { adj.assign(N, vector<int>()); }
// 3
void addEdge(int u, int v) {
  adj[u].push_back(v), adj[v].push_back(u);
}
// 10
// O(N)
void eulerTour(int u, int p, int h) {
  tin[u] = tour.size();
  tour.push_back({h, u});
  for (int v : adj[u])
    if (v != p) {
      eulerTour(v, u, h + 1);
      tour.push_back({h, u});
    }
}
// 10
// O(N * lg(N))
void preprocess(const vector<int> &roots) {
  tour.clear();
  tin.assign(adj.size(), -1);
  for (auto &root : roots) eulerTour(root, root, 0);
  st = SparseTable<pairii>(
      tour, [](pairii a, pairii b) {
        return a.first < b.first ? a : b;
      });
}
// 6
// O(1)
int lca(int u, int v) {
  int l = min(tin[u], tin[v]);
  int r = max(tin[u], tin[v]);
  return st.query(l, r).second;
}