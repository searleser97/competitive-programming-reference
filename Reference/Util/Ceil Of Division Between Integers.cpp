// 6
//         a / b
li ceil(li a, li b) {
  auto sign = [](li x) { return x < 0 ? -1 : x > 0; };
  if (sign(a) == sign(b) && a % b) return a / b + 1;
  else return a / b;
}