// 15
// all possible ways to choose elements from a given set

// O(2^n * n)
template<class T>
vector<vector<T>> combinations(const vector<T>& v) {
  int limit = 1 << int(v.size());
  vector<vector<T>> ans;
  for (int i = 0; i < limit; i++) {
    vector<T> tmp;
    for (int j = 0; j < int(v.size()); j++) {
      if (i & (1 << j)) {
        tmp.push_back(v[j]);
      }
    }
    ans.push_back(tmp);
  }
  return ans;
}